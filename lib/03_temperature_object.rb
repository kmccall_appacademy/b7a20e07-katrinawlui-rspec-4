class Temperature
  attr_reader :temperature

  def self.from_celsius(temp)
    self.new(c: temp)
  end

  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end

  def ctof(temp)
    temp * 9.fdiv(5) + 32
  end

  def ftoc(temp)
    (temp - 32) * 5.fdiv(9)
  end

  def initialize(options = {})
    defaults = {
      f: nil,
      c: nil
    }
    @temperature = defaults.merge(options)
  end

  def in_fahrenheit
    if temperature[:f].nil?
      temperature[:f] = ctof(temperature[:c])
    else
      temperature[:f]
    end
  end

  def in_celsius
    if temperature[:c].nil?
      temperature[:c] = ftoc(temperature[:f])
    else
      temperature[:c]
    end
  end
end

class Celsius < Temperature
  def initialize(temp)
    @temperature = { c: temp }
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @temperature = { f: temp }
  end
end
