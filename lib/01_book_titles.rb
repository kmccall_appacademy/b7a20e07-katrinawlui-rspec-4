class Book
  attr_accessor :title

  def title
    exceptions = %w[the a an and in the of]
    words = @title.split.map(&:downcase)

    words.each_with_index do |word, i|
      words[i] = word.capitalize if i == 0 || !exceptions.include?(word)
    end

    @title = words.join(" ")
  end
end
