class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    "#{padded(hours)}:#{padded(minutes)}:#{padded(minute_seconds)}"
  end

  def padded(num)
    if num.to_s.length == 1
      "0#{num}"
    else
      num.to_s
    end
  end

  def hours
    seconds / 3600
  end

  def minutes
    (seconds % 3600) / 60
  end

  def minute_seconds
    seconds % 60
  end
end
