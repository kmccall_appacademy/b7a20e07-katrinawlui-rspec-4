class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def keywords
    entries.keys.sort
  end

  def add(entry)
    if entry.is_a?(Hash)
      entries.merge!(entry)
    elsif entry.is_a?(String)
      entries[entry] = nil
    end
  end

  def include?(keyword)
    entries.key?(keyword)
  end

  def find(fragment)
    entries.select { |k, v| k.include?(fragment) }
  end

  def printable
    printable = keywords.map { |k| "[#{k}] \"#{entries[k]}\"" }
    printable.join("\n")
  end
end
